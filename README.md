## Instrucciones para ejecutar el programa

1. Descargar el repositorio completo.
2. Descomprimir el repositorio y situarlo en cualquier carpeta.
3. Abrir archivo index.html con Google Chrome o Edge
4. El programa ha sido probado con Chrome Versión 94.0.4606.61 (Build oficial) (64 bits) y Edge Versión 94.0.992.31 (Compilación oficial) (64 bits)
5. No requiere de conexión a internet
6. No se necesita nada más para hacerlo funcionar

---

## Notas importantes sobre el desarrollo

1. Debido a la naturaleza de la oferta de trabajo, he tratado de utilizar SIEMPRE javascript nativo con HTML, CSS y Bootstrap. Todo a pesar de que en el PDF se permitían frameworks de los que también tengo conocimiento.
2. He realizado un análisis estático del código JS en https://jshint.com/ y todo está correcto.
3. He realizado diversos tests para verificar el correcto funcionamiento de las funciones más críticas del programa
4. He añadido funcionalidad extra al programa: Crear usuarios, comprobación de inicio de sesión, borrar usuario/s, ver lista de usuarios, cambiar contraseña y un (muy) sencillo sistema de permisos con rol de administrador y usuario estándar.
5. Para maquetar he utilizado principalmente bootstrap.
6. Todo el código está comentado pero cualquier duda estoy encantado de resolverla

¡Gracias por su tiempo e interés!