window.onload = function() {
    inicializar(); //Inicializamos todo lo necesario y comprobamos el login
};


function inicializar() { //Función que inicializa eventos click, inicia el intervalo y checkea el login del usuario
    var usuario = sessionStorage.getItem("logueado");
    if (usuario == null) { //No está logueado
        window.location.href = "index.html";
    } else {
        //Inicializamos todos los listeners de evento click
        document.getElementById("cerrarSesion").addEventListener('click', cerrarSesion);
        document.getElementById("borrarUsuario").addEventListener('click', borrarUsuario);
        document.getElementById("borrarTodosUsuarios").addEventListener('click', borrarTodosUsuarios);
        document.getElementById("cambiarClave").addEventListener('click', cambiarClave);
        document.getElementById("mostrarOcultarListaUsuarios").addEventListener('click', mostrarOcultarListaUsuarios);        
        document.getElementById("divBienvenido").innerHTML = "Bienvenido/a, "+usuario+"!";

        var busquedaFechaLogin = localStorage.getItem(usuario+"_tiempoLogin"); //Buscamos la última vez que inició sesión

        if (busquedaFechaLogin == null) { //Es la primera vez que se logea
            document.getElementById("tiempoLogin").innerHTML = "Es tu primera vez!";
        } else { //Ya inició sesión con anterioridad
            actualizarContador(busquedaFechaLogin); //Mostramos el contador
            window.setInterval(actualizarContador, 1000, busquedaFechaLogin); //Lanzamos un intervalo cada segundo para que se vaya actualizando el contador
        }

        localStorage.setItem(usuario+"_tiempoLogin", getFechaActual()); //Actualizamos el último inicio de sesión con la fecha y hora actual

        var admin = localStorage.getItem(usuario+"_admin"); //Buscamos el tipo de usuario que ha hecho login
        if (admin == "true") { //Si el usuario es administrador
            rellenaListaUsuarios(); //Rellenamos la lista de usuarios registrados

        } else { //Si el usuario NO es administrador ocultamos lo que no debe ver (aunque internamente tiene más controles)
            document.getElementById("rowListaUsuarios").style.display = "none";
            document.getElementById("rowBotonBorrarTodosUsuarios").style.display = "none";
        }
    }
}

function actualizarContador(fechaAntigua) { //Actualiza el contador del tiempo del último login mostrado en pantalla
    var FechaActual = getFechaActual(); //Obtenemos la fecha actual
    var diferenciaTiempo = CalcularDiferenciaFechas(new Date(fechaAntigua), new Date(FechaActual)); //Calculamos la diferencia con la fecha del último login
    document.getElementById("tiempoLogin").innerHTML = diferenciaTiempo; //Mostramos la diferencia en pantalla
}

function cerrarSesion() { //Función que se ejecuta cuando se envía el formulario de cerrar sesión
    
    sessionStorage.removeItem('logueado'); //Eliminamos la variable de sesión que marca que el usuario está logueado
    window.location.href = "index.html"; //Le llevamos a la página de login.html
}

function borrarUsuario() { //Borra el usuario actual
    var usuario = sessionStorage.getItem("logueado");

    if (confirm("Estás seguro de borrar tu usuario?")) {
        localStorage.removeItem(usuario+"_usuario");
        localStorage.removeItem(usuario+"_clave");
        localStorage.removeItem(usuario+"_tiempoLogin");
        sessionStorage.removeItem('logueado');
        
        window.location.href = "index.html"; //Le llevamos a la página de login.html
        alert("Usuario borrado correctamente. Se te devolverá a la pantalla de inicio de sesión.");
    }
}

function borrarTodosUsuarios() { //Borra todos los usuarios
    var usuario = sessionStorage.getItem("logueado");
    var admin = localStorage.getItem(usuario+"_admin"); //Buscamos el tipo de usuario que ha hecho login
    if (admin == "true") { //Si el usuario es administrador
        if (confirm("Estás seguro de borrar todos los usuarios?")) {
            localStorage.clear();
            sessionStorage.removeItem('logueado');
            window.location.href = "index.html"; //Le llevamos a la página de login.html
            alert("Usuarios borrados correctamente. Se te devolverá a la pantalla de inicio de sesión.");
        }
    }
}


function rellenaListaUsuarios() { //Rellena la lista de usuarios

    //obtenemos todas las variables de tipo localStorage
    var values = [],
        keys = Object.keys(localStorage),
        i = keys.length;

    //Recorremos las variabels y nos quedamos con las que son de tipo "usuario"
    while ( i-- ) {
        var tipo = keys[i];
        if (tipo.indexOf("_usuario") !== -1) {
            values.push( localStorage.getItem(keys[i]) );
        }

    }
    //Creamos la lista de usuarios con código html
    var lista = "";
    for (i=0; i<values.length; i++) {
        if (i>0) lista += "<br>";
        lista+= values[i];
    }

    //Insertamos el HTML en el div contenedor de la lista de usuarios
    document.getElementById("listaUsuarios").innerHTML = lista;
}

function mostrarOcultarListaUsuarios() { //Muestra u oculta la lista de usuarios cuando se pulsa en el botón correspondiente
    if (document.getElementById("listaUsuarios").style.display == "none") { 
        document.getElementById("listaUsuarios").style.display = "";
        document.getElementById("mostrarOcultarListaUsuarios").value = "Ocultar lista de usuarios";

    } else {
        document.getElementById("listaUsuarios").style.display = "none";
        document.getElementById("mostrarOcultarListaUsuarios").value = "Mostrar lista de usuarios";
    } 

}
function cambiarClave() { //Cambiar la clave del usuario
    var nuevaclave = prompt('Introduce la nueva clave');
    if (nuevaclave == "" || nuevaclave == null) { //Si la clave está vacía o el usuario ha pulsado "cancelar"
        alert("Has introducido una clave vacía");
        return;
    }
    var nuevaclave2 = prompt('Repite la nueva clave');
    if (nuevaclave != nuevaclave2) { //Si la repetición de la clave no coincide con la primera
        alert("Las claves no coindicen. No se ha ejecutado la actualización");
        return;
    }

    var usuario = sessionStorage.getItem("logueado"); //Obtenemos el usuario logueado
    localStorage.setItem(usuario+"_clave", nuevaclave); //Actualizamos la clave del usuario
    alert("Clave actualizada correctamente!");


}
function getFechaActual() { //Convierte y devuelve la fecha actual en formato YYYY-mm-dd HH:mm:ss
    var fechaAux = new Date();

    var dia = ("0" + fechaAux.getDate()).slice(-2);
    var mes = ("0" + (fechaAux.getMonth() + 1)).slice(-2);
    var ano = fechaAux.getFullYear();
    var horas = fechaAux.getHours();
    var minutos = fechaAux.getMinutes();
    var segundos = fechaAux.getSeconds();

    return(ano + "-" + mes + "-" + dia + " " + horas + ":" + minutos + ":" + segundos);

}
function CalcularDiferenciaFechas(fechaNueva, fechaVieja) { //Calcula y devuelve la diferencia entre dos fechas en días, horas, minutos y segundos
    var diferenciaEnSegundos = Math.abs(fechaNueva - fechaVieja) / 1000; //Restamos las fechas y las convertimos en segundos

    // calcula dias
    var dias = Math.floor(diferenciaEnSegundos / 86400);
    diferenciaEnSegundos -= dias * 86400;

    // calcula horas
    var horas = Math.floor(diferenciaEnSegundos / 3600) % 24;
    diferenciaEnSegundos -= horas * 3600;

    // calcula minutos
    var minutos = Math.floor(diferenciaEnSegundos / 60) % 60;
    diferenciaEnSegundos -= minutos * 60;

    // calcula segundos
    var segundos = diferenciaEnSegundos; //Lo que resta son solo segundos

    var diferencia = ''; //Generamos la cadena que muestra el tiempo transcurrido
    diferencia += dias + " dias ";

    diferencia += horas + " horas ";

    diferencia += minutos + " minutos ";
    
    diferencia += segundos + " segundos "; 

    return "<div class='badge bg-highlight text-wrap'>"+diferencia+"</div>";
  }