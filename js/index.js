window.onload = function() {
    inicializar(); //Inicializamos todo lo necesario y comprobamos el login
};


function inicializar() {
    var logueado = sessionStorage.getItem("logueado"); //Recogemos variable que nos indica si el usuario está logueado o no
    if (logueado != null) { //Está logueado
        window.location.href = "logueado.html"; //Le redirigimos directamente
    } else { //No está logueado
        //Inicializamos todos los listeners de evento click
        document.getElementById("formLogin").addEventListener('submit', doLogin); //Evento cuando se envía el formulario
        document.getElementById("crearUsuario").addEventListener('click', crearUsuario); //Evento cuando se pulsa "Crear usuario"        
    }
}

function doLogin() { //Función que se ejecuta cuando se envía el formulario de login

    var usuario = document.getElementById("usuario").value; //Recogemos el usuario introducido
    var busquedaUsuario = localStorage.getItem(usuario+"_usuario"); //Buscamos el usuario entre los existentes

    if (busquedaUsuario != null) { //Existe el usuario
        if (document.getElementById("clave").value == localStorage.getItem(usuario+"_clave")) { //Si la clave es correcta
            sessionStorage.setItem("logueado", usuario); //Guardamos variable de sesión de logueado OK
            window.location.href = "logueado.html"; //Le llevamos a la página de logueado.html
        } else { //Error de login
            alert("Error: Clave incorrecta.");
        }
    }  else { //Error de login
        alert("Error: El usuario no existe.");
    }
}
function crearUsuario() { //Función que se ejecuta cuando se envía el formulario de login
    
    var usuario = document.getElementById("usuario").value; //Recogemos el usuario introducido
    var clave = document.getElementById("clave").value; //Recogemos el usuario introducido
    if (usuario == "") {
        alert("Debe escribir un nombre de usuario");
        document.getElementById("usuario").focus();
        return false;
    }
    if (clave == "") {
        alert("Debe escribir una clave de acceso");
        document.getElementById("clave").focus();
        return false;
    }    

    var busquedaUsuario = localStorage.getItem(usuario+"_usuario"); //Buscamos el usuario entre los existentes

    if (busquedaUsuario == null) { //No existe el usuario así que lo creamos 
        sessionStorage.setItem("logueado", usuario); //Guardamos variable de sesión de logueado OK
        localStorage.setItem(usuario+"_usuario", usuario); //Guardamos variable de nombre de usuario
        localStorage.setItem(usuario+"_clave", clave); //Guardamos variable de clave de usuario
        if (confirm("Desea que el usuario sea de tipo 'Administrador'?")) {
            localStorage.setItem(usuario+"_admin", "true");
        } else {
            localStorage.setItem(usuario+"_admin", "false");
        }

        alert("Usuario creado correctamente. El login se realizará de forma automática.");
        window.location.href = "logueado.html"; //Le llevamos a la página de logueado.html
    } else {
        alert("El usuario ya existe. Por favor, elija un nombre de usuario diferente.");
    }
}